import java.util.*;

public class Die
{
	private int pips;
	private Random hasard;
 

	public Die()
	{	
		pips = 1;
		hasard = new Random();
		}

	public int getPips()
	{
		return this.pips;
		}

	public int roll()
	{
		return pips = hasard.nextInt(6)+1;
		}

	public String toString()
	{
		return "The pips is "+this.pips;
		}
 }
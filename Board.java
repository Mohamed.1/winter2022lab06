public class Board
{
	//Private Fields
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	//Constructor
	public Board()
	{
		die1 = new Die();
		die2 = new Die();
		closedTiles = new boolean[12];
		 }

	//Step 4 - Part 2
	public String toString()
	{ 	
		String closedTilesString = "";
		for(int i = 0; i < closedTiles.length; i++)
		{
			if(this.closedTiles[i] == false)
			{ closedTilesString = closedTilesString+" "+(i+1);}

			else
			{ closedTilesString = closedTilesString+" X ";}
		}
		return closedTilesString;
	}

	//Step 5 - Part 2
	public boolean playATurn()
	{ 
		die1.roll();
		die2.roll();
		System.out.println("Result of die1: "+die1);
		System.out.println("Result of die2: "+die2);
		int sum = die1.getPips() + die2.getPips();
		boolean position;
		int p = sum-1;
		if(this.closedTiles[p] == false)
		{ 
			this.closedTiles[p] = true;
			System.out.println("Closing tile at position: "+p);
			position = false; 
			}
		else
		{ 
			System.out.println("This position is already shut");
			position = true;
			}
		return position;
	 }	
 }